namespace tech_test_payment_api.Models
{
    public enum EnumStatusVenda
    {
        PagamentoAprovado,
        EnviadoParaTransportadora,
        Entregue,
        Cancelada
    }
}