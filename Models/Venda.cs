using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Models
{
    
    public class Venda
    {
        public int Id { get; set; }
        public string NomeVendedor { get; set; }
        public string CpfVendedor { get; set; }
        public string EmailVendedor { get; set; }   
        public string TelefoneVendedor { get; set; } 
        public DateTime DataVenda { get; set; }
        public int IdPedido { get; set; }
        public string ItensVenda { get; set; }
        public EnumStatusVenda StatusVenda { get; set; }
        
    }
}